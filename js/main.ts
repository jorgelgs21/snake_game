class Figura {
    public    ancho_canvas  :number;
    public    alto_canvas   :number;
    public    posicion_x    :number;
    public    posicion_y    :number;
    protected width_size    :number = 10;
    protected height_size   :number = 10;
    protected color         :string;
    protected ctx           :CanvasRenderingContext2D

    constructor(    ctx         :any,
                    posicion_x  :number,
                    posicion_y  :number,
                    color       :string = 'green'){
        this.posicion_x     = posicion_x;
        this.posicion_y     = posicion_y;
        this.color          = color;
        this.ctx            = ctx
        this.ancho_canvas   = ctx.canvas.width
        this.alto_canvas    = ctx.canvas.height
    }

    dibujarRect(x:number ,y:number ,w:number ,h:number){
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(x,y,w,h)
    }

    dibujarTexto(texto:string, x:number ,y:number, max_w:number = 80, fuente:string = "20px Cambria"){
        this.ctx.font = fuente
        this.ctx.fillText(texto, x, y, max_w)
    }

    borrarRect(x:number ,y:number ,w:number ,h:number){
        this.ctx.clearRect(x,y,w,h)
    }

    borrarTodo(){
        this.ctx.clearRect(0, 0, this.ancho_canvas, this.alto_canvas)
    }

    choque(obj:Figura){
        let diferencia_x = Math.abs(this.posicion_x - obj.posicion_x);
        let diferencia_y = Math.abs(this.posicion_y - obj.posicion_y);
        if (diferencia_x == 0 && diferencia_y == 0) {
            return true
        }
        return false
    }
}

class Snake extends Figura {
    direccion_x     :number = 0;
    direccion_y     :number = 0;
    in_eje_x        :boolean = true;
    in_eje_y        :boolean = true;
    cola            :Snake   = null;

    constructor(ctx,pos_x:number, pos_y:number, color:string = 'black'){
        super(ctx,pos_x,pos_y,color)
    }

    dibujar(){
        if (this.cola != null) {
            this.cola.dibujar()
        }
        this.dibujarRect(   this.posicion_x, 
                            this.posicion_y, 
                            this.width_size, 
                            this.height_size)
    }

    borrar(){
        if (this.cola != null) {
            this.cola.borrar()
        }
        this.borrarRect(   this.posicion_x, 
                            this.posicion_y, 
                            this.width_size, 
                            this.height_size)
    }
    
    actualizar(){
        let nx = this.posicion_x + this.direccion_x;
        let ny = this.posicion_y + this.direccion_y
        this.setXY(nx, ny)
    }
    
    setXY(new_x:number,new_y:number){
        if (this.cola != null) {
            this.cola.setXY(this.posicion_x, this.posicion_y)
        }
        this.posicion_x = new_x;
        this.posicion_y = new_y;
    }

    crecer(){
        if (this.cola == null) {
            this.cola = new Snake(this.ctx, this.posicion_x, this.posicion_y, 'green')
        }else{
            this.cola.crecer()
        }
    }

    getCola(){
        return this.cola;
    }

    choqueCuerpo(){
        let temporal = null;
        try {
            temporal = this.getCola().getCola()
        } catch (error) {
            temporal = null
        }
        while (temporal != null) {
            if (this.choque(temporal)) {
                return true
            }else{
                temporal = temporal.getCola()
            }
        }
    }

    choqueParedModoLibre(){
        if (this.posicion_x < 0) {
            this.posicion_x = this.ancho_canvas - this.width_size
        }
        if (this.posicion_x > this.ancho_canvas - this.width_size){
            this.posicion_x = 0
        }
        if (this.posicion_y < 0) {
            this.posicion_y = this.alto_canvas - this.height_size
        }
        if (this.posicion_y > this.alto_canvas - this.height_size){
            this.posicion_y = 0
        }
    }

    choquePared(){
        if (this.posicion_x < 0 || this.posicion_x > (this.ancho_canvas  - this.width_size) ||
            this.posicion_y < 0 || this.posicion_y > (this.alto_canvas - this.height_size)) {
            return true
        }
    }

    moveTo(direccion:string){
        if (this.in_eje_x) {
            if (direccion == 'derecha') {
                this.direccion_x    = this.width_size;
                this.direccion_y    = 0
                this.in_eje_x       = false;
                this.in_eje_y       = true;
            }
            if (direccion == 'izquierda') {
                this.direccion_x    = -this.width_size;
                this.direccion_y    = 0
                this.in_eje_x       = false;
                this.in_eje_y       = true;                
            }
        }
        if (this.in_eje_y) {
            if (direccion == 'abajo') {
                this.direccion_y    = this.width_size;
                this.direccion_x    = 0
                this.in_eje_y       = false;
                this.in_eje_x       = true;                  
            }
            if (direccion == 'arriba') {
                this.direccion_y    = -this.width_size;
                this.direccion_x    = 0
                this.in_eje_y       = false;
                this.in_eje_x       = true;                 
            }            
        }
    }

}

class Comida extends Figura {
    constructor(ctx){
        super(ctx,0,0,'red')
        this.colocarAleatorio()
    }

    dibujar(){
        this.dibujarRect(   this.posicion_x, 
                            this.posicion_y, 
                            this.width_size, 
                            this.height_size)
    }

    colocarAleatorio(){
        this.posicion_x = Math.abs(Math.floor(Math.random() * (this.ancho_canvas / this.width_size)) * this.width_size)
        this.posicion_y = Math.abs(Math.floor(Math.random() * (this.alto_canvas / this.height_size)) * this.height_size)
    }
}

class Puntos extends Figura {
    private puntaje:number = 0;
    constructor(ctx, pos_x:number, pos_y:number, color:string = 'black'){
        super(ctx, pos_x,pos_y,color)
        this.width_size = 80;
    }

    getPuntos(){
        return this.puntaje;
    }

    dibujarPuntaje(){
        this.dibujarTexto('Puntos: ' + this.puntaje, this.posicion_x, this.posicion_y + this.height_size, this.width_size)
    }

    borrarPuntaje(){
        this.borrarRect(this.posicion_x, 0, this.width_size, this.height_size * 2)
    }
    
    subirPunto(incrementar:number){
        let extra:number = (incrementar == 3) ? 2 : 0
        this.puntaje += (incrementar + extra)
    }
}

class Nivel extends Figura {
    private nivel:number = 10;

    constructor(ctx, pos_x:number, pos_y:number, color:string = 'black'){
        super(ctx, pos_x,pos_y,color)
        this.width_size = 80;
    }

    getNivel(){
        return this.nivel;
    }

    dibujarNivel(){
        let n:string
        if (this.nivel == 10) n = 'Bajo';
        if (this.nivel == 20) n = 'Medio';
        if (this.nivel == 30) n = 'Alto';
        this.dibujarTexto('Nivel: ' + n, this.posicion_x, this.posicion_y + this.height_size, this.width_size)
    }

    borrarNivel(){
        this.borrarRect(this.posicion_x, 0, this.width_size, this.height_size * 2)
    }
    
    setNivel(nivel:number){
        this.nivel = nivel
    }
}

class Juego {
    private snake        :Snake;
    private comida       :Comida;
    private puntos       :Puntos;
    private nivel        :Nivel;
    public  playing      :boolean = false;
    public  speed        :number;

    constructor(ctx,speed:number = 10){
        this.speed  = speed
        this.snake  = new Snake(ctx,20,20);
        this.comida = new Comida(ctx)
        this.puntos = new Puntos(ctx,20,10)
        this.nivel  = new Nivel(ctx,this.snake.ancho_canvas - 100 ,10)
    }

    setNivel(){
        this.nivel.setNivel(this.speed)
    }

    limpiar(){
        this.snake.borrar()
        this.puntos.borrarPuntaje()
        this.nivel.borrarNivel()
    }

    dibujar(){
        this.snake.dibujar()
        this.puntos.dibujarPuntaje()
        this.nivel.dibujarNivel()
        this.comida.dibujar()    
    }
    actualizar(){
        this.limpiar()
        this.snake.actualizar()        
    }

    finJuego(){
        location.reload(true)
        alert('perdiste')

        let puntaje_actual = this.puntos.getPuntos().toString();
        localStorage.setItem('puntaje_actual',puntaje_actual);
        let maximo_puntaje = localStorage.getItem('maxima_puntuacion')
        if (maximo_puntaje) {
            if (parseInt(puntaje_actual) > parseInt(maximo_puntaje)) {
                localStorage.setItem('nombre',prompt('Felicidades \n Mejoraste el Puntaje \n Ingresa tu Nombre'))
                localStorage.setItem('maxima_puntuacion',puntaje_actual)
            }
        }else{
            localStorage.setItem('nombre',prompt('Felicidades \n Ingresa tu Nombre'))
            localStorage.setItem('maxima_puntuacion',puntaje_actual)
        }
    }
    
    chocar(){
        if(this.snake.choqueCuerpo() || this.snake.choquePared()){
            this.finJuego()
        }

        if (this.snake.choque(this.comida)) {
            this.comida.colocarAleatorio()
            this.snake.crecer()
            this.puntos.subirPunto(this.nivel.getNivel() / 10)
        }

    }

    controlarTeclado(event:KeyboardEvent){
        switch (event.keyCode) {
            case 37: this.snake.moveTo('izquierda') ; break;
            case 39: this.snake.moveTo('derecha')   ; break;
            case 38: this.snake.moveTo('arriba')    ; break;
            case 40: this.snake.moveTo('abajo')     ; break;
        }
    }

}

function cambioNivel(input_nivel){
    
    if (!juego.playing) {
        switch (input_nivel.value) {
            case 'bajo' : juego.speed = 10 ; break;
            case 'medio': juego.speed = 20 ; break;
            case 'alto' : juego.speed = 30 ; break;
        }
        localStorage.setItem('nivel', input_nivel.value)
        localStorage.setItem('speed', juego.speed.toString())
    }
}

function play() {
    let speed = parseInt(localStorage.getItem('speed'))
    if (speed) {
        juego.speed = speed
    }
    if (!juego.playing) {
        animationFrame(juego.speed)
        juego.playing = true
        juego.setNivel()
        let niveles:any = document.getElementsByName('nivel');
        for (let i = 0; i < niveles.length; i++) {
            niveles[i].disabled = true
        }
    }
}


function animationFrame(vel) {
    setInterval(()=>{
        juego.actualizar()
        juego.chocar()
        juego.dibujar()
    },1000 / vel)
}


let canvas  :any        = document.getElementById('canvas')
let contexto:CanvasRenderingContext2D = canvas.getContext('2d')
let juego:Juego         = new Juego(contexto)
let nivel               = localStorage.getItem('nivel')
let nombre              = localStorage.getItem('nombre')
let maximo_puntaje      = localStorage.getItem('maxima_puntuacion')
let puntaje_anterior    = localStorage.getItem('puntaje_actual')

if (nivel) {
    let input:any = document.getElementById(nivel)
    input.checked = true
}

if (puntaje_anterior){

    document.getElementById('maxima_puntuacion').innerHTML = '<b>La Maxima Puntuacion: </b></br>' 
    + nombre + ' con ' + maximo_puntaje + ' Puntos';

    document.getElementById('ultima_puntuacion').innerHTML = '<b>La Ultima Puntuacion fue de: </br></b>'
    + puntaje_anterior + ' Puntos';
}

document.addEventListener('keydown',(event)=>{
    juego.controlarTeclado(event)
})
