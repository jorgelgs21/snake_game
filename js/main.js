var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Figura = /** @class */ (function () {
    function Figura(ctx, posicion_x, posicion_y, color) {
        if (color === void 0) { color = 'green'; }
        this.width_size = 10;
        this.height_size = 10;
        this.posicion_x = posicion_x;
        this.posicion_y = posicion_y;
        this.color = color;
        this.ctx = ctx;
        this.ancho_canvas = ctx.canvas.width;
        this.alto_canvas = ctx.canvas.height;
    }
    Figura.prototype.dibujarRect = function (x, y, w, h) {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(x, y, w, h);
    };
    Figura.prototype.dibujarTexto = function (texto, x, y, max_w, fuente) {
        if (max_w === void 0) { max_w = 80; }
        if (fuente === void 0) { fuente = "20px Cambria"; }
        this.ctx.font = fuente;
        this.ctx.fillText(texto, x, y, max_w);
    };
    Figura.prototype.borrarRect = function (x, y, w, h) {
        this.ctx.clearRect(x, y, w, h);
    };
    Figura.prototype.borrarTodo = function () {
        this.ctx.clearRect(0, 0, this.ancho_canvas, this.alto_canvas);
    };
    Figura.prototype.choque = function (obj) {
        var diferencia_x = Math.abs(this.posicion_x - obj.posicion_x);
        var diferencia_y = Math.abs(this.posicion_y - obj.posicion_y);
        if (diferencia_x == 0 && diferencia_y == 0) {
            return true;
        }
        return false;
    };
    return Figura;
}());
var Snake = /** @class */ (function (_super) {
    __extends(Snake, _super);
    function Snake(ctx, pos_x, pos_y, color) {
        if (color === void 0) { color = 'black'; }
        var _this = _super.call(this, ctx, pos_x, pos_y, color) || this;
        _this.direccion_x = 0;
        _this.direccion_y = 0;
        _this.in_eje_x = true;
        _this.in_eje_y = true;
        _this.cola = null;
        return _this;
    }
    Snake.prototype.dibujar = function () {
        if (this.cola != null) {
            this.cola.dibujar();
        }
        this.dibujarRect(this.posicion_x, this.posicion_y, this.width_size, this.height_size);
    };
    Snake.prototype.borrar = function () {
        if (this.cola != null) {
            this.cola.borrar();
        }
        this.borrarRect(this.posicion_x, this.posicion_y, this.width_size, this.height_size);
    };
    Snake.prototype.actualizar = function () {
        var nx = this.posicion_x + this.direccion_x;
        var ny = this.posicion_y + this.direccion_y;
        this.setXY(nx, ny);
    };
    Snake.prototype.setXY = function (new_x, new_y) {
        if (this.cola != null) {
            this.cola.setXY(this.posicion_x, this.posicion_y);
        }
        this.posicion_x = new_x;
        this.posicion_y = new_y;
    };
    Snake.prototype.crecer = function () {
        if (this.cola == null) {
            this.cola = new Snake(this.ctx, this.posicion_x, this.posicion_y, 'green');
        }
        else {
            this.cola.crecer();
        }
    };
    Snake.prototype.getCola = function () {
        return this.cola;
    };
    Snake.prototype.choqueCuerpo = function () {
        var temporal = null;
        try {
            temporal = this.getCola().getCola();
        }
        catch (error) {
            temporal = null;
        }
        while (temporal != null) {
            if (this.choque(temporal)) {
                return true;
            }
            else {
                temporal = temporal.getCola();
            }
        }
    };
    Snake.prototype.choqueParedModoLibre = function () {
        if (this.posicion_x < 0) {
            this.posicion_x = this.ancho_canvas - this.width_size;
        }
        if (this.posicion_x > this.ancho_canvas - this.width_size) {
            this.posicion_x = 0;
        }
        if (this.posicion_y < 0) {
            this.posicion_y = this.alto_canvas - this.height_size;
        }
        if (this.posicion_y > this.alto_canvas - this.height_size) {
            this.posicion_y = 0;
        }
    };
    Snake.prototype.choquePared = function () {
        if (this.posicion_x < 0 || this.posicion_x > (this.ancho_canvas - this.width_size) ||
            this.posicion_y < 0 || this.posicion_y > (this.alto_canvas - this.height_size)) {
            return true;
        }
    };
    Snake.prototype.moveTo = function (direccion) {
        if (this.in_eje_x) {
            if (direccion == 'derecha') {
                this.direccion_x = this.width_size;
                this.direccion_y = 0;
                this.in_eje_x = false;
                this.in_eje_y = true;
            }
            if (direccion == 'izquierda') {
                this.direccion_x = -this.width_size;
                this.direccion_y = 0;
                this.in_eje_x = false;
                this.in_eje_y = true;
            }
        }
        if (this.in_eje_y) {
            if (direccion == 'abajo') {
                this.direccion_y = this.width_size;
                this.direccion_x = 0;
                this.in_eje_y = false;
                this.in_eje_x = true;
            }
            if (direccion == 'arriba') {
                this.direccion_y = -this.width_size;
                this.direccion_x = 0;
                this.in_eje_y = false;
                this.in_eje_x = true;
            }
        }
    };
    return Snake;
}(Figura));
var Comida = /** @class */ (function (_super) {
    __extends(Comida, _super);
    function Comida(ctx) {
        var _this = _super.call(this, ctx, 0, 0, 'red') || this;
        _this.colocarAleatorio();
        return _this;
    }
    Comida.prototype.dibujar = function () {
        this.dibujarRect(this.posicion_x, this.posicion_y, this.width_size, this.height_size);
    };
    Comida.prototype.colocarAleatorio = function () {
        this.posicion_x = Math.abs(Math.floor(Math.random() * (this.ancho_canvas / this.width_size)) * this.width_size);
        this.posicion_y = Math.abs(Math.floor(Math.random() * (this.alto_canvas / this.height_size)) * this.height_size);
    };
    return Comida;
}(Figura));
var Puntos = /** @class */ (function (_super) {
    __extends(Puntos, _super);
    function Puntos(ctx, pos_x, pos_y, color) {
        if (color === void 0) { color = 'black'; }
        var _this = _super.call(this, ctx, pos_x, pos_y, color) || this;
        _this.puntaje = 0;
        _this.width_size = 80;
        return _this;
    }
    Puntos.prototype.getPuntos = function () {
        return this.puntaje;
    };
    Puntos.prototype.dibujarPuntaje = function () {
        this.dibujarTexto('Puntos: ' + this.puntaje, this.posicion_x, this.posicion_y + this.height_size, this.width_size);
    };
    Puntos.prototype.borrarPuntaje = function () {
        this.borrarRect(this.posicion_x, 0, this.width_size, this.height_size * 2);
    };
    Puntos.prototype.subirPunto = function (incrementar) {
        var extra = (incrementar == 3) ? 2 : 0;
        this.puntaje += (incrementar + extra);
    };
    return Puntos;
}(Figura));
var Nivel = /** @class */ (function (_super) {
    __extends(Nivel, _super);
    function Nivel(ctx, pos_x, pos_y, color) {
        if (color === void 0) { color = 'black'; }
        var _this = _super.call(this, ctx, pos_x, pos_y, color) || this;
        _this.nivel = 10;
        _this.width_size = 80;
        return _this;
    }
    Nivel.prototype.getNivel = function () {
        return this.nivel;
    };
    Nivel.prototype.dibujarNivel = function () {
        var n;
        if (this.nivel == 10)
            n = 'Bajo';
        if (this.nivel == 20)
            n = 'Medio';
        if (this.nivel == 30)
            n = 'Alto';
        this.dibujarTexto('Nivel: ' + n, this.posicion_x, this.posicion_y + this.height_size, this.width_size);
    };
    Nivel.prototype.borrarNivel = function () {
        this.borrarRect(this.posicion_x, 0, this.width_size, this.height_size * 2);
    };
    Nivel.prototype.setNivel = function (nivel) {
        this.nivel = nivel;
    };
    return Nivel;
}(Figura));
var Juego = /** @class */ (function () {
    function Juego(ctx, speed) {
        if (speed === void 0) { speed = 10; }
        this.playing = false;
        this.speed = speed;
        this.snake = new Snake(ctx, 20, 20);
        this.comida = new Comida(ctx);
        this.puntos = new Puntos(ctx, 20, 10);
        this.nivel = new Nivel(ctx, this.snake.ancho_canvas - 100, 10);
    }
    Juego.prototype.setNivel = function () {
        this.nivel.setNivel(this.speed);
    };
    Juego.prototype.limpiar = function () {
        this.snake.borrar();
        this.puntos.borrarPuntaje();
        this.nivel.borrarNivel();
    };
    Juego.prototype.dibujar = function () {
        this.snake.dibujar();
        this.puntos.dibujarPuntaje();
        this.nivel.dibujarNivel();
        this.comida.dibujar();
    };
    Juego.prototype.actualizar = function () {
        this.limpiar();
        this.snake.actualizar();
    };
    Juego.prototype.finJuego = function () {
        location.reload(true);
        alert('perdiste');
        var puntaje_actual = this.puntos.getPuntos().toString();
        localStorage.setItem('puntaje_actual', puntaje_actual);
        var maximo_puntaje = localStorage.getItem('maxima_puntuacion');
        if (maximo_puntaje) {
            if (parseInt(puntaje_actual) > parseInt(maximo_puntaje)) {
                localStorage.setItem('nombre', prompt('Felicidades \n Mejoraste el Puntaje \n Ingresa tu Nombre'));
                localStorage.setItem('maxima_puntuacion', puntaje_actual);
            }
        }
        else {
            localStorage.setItem('nombre', prompt('Felicidades \n Ingresa tu Nombre'));
            localStorage.setItem('maxima_puntuacion', puntaje_actual);
        }
    };
    Juego.prototype.chocar = function () {
        if (this.snake.choqueCuerpo() || this.snake.choquePared()) {
            this.finJuego();
        }
        if (this.snake.choque(this.comida)) {
            this.comida.colocarAleatorio();
            this.snake.crecer();
            this.puntos.subirPunto(this.nivel.getNivel() / 10);
        }
    };
    Juego.prototype.controlarTeclado = function (event) {
        switch (event.keyCode) {
            case 37:
                this.snake.moveTo('izquierda');
                break;
            case 39:
                this.snake.moveTo('derecha');
                break;
            case 38:
                this.snake.moveTo('arriba');
                break;
            case 40:
                this.snake.moveTo('abajo');
                break;
        }
    };
    return Juego;
}());
function cambioNivel(input_nivel) {
    if (!juego.playing) {
        switch (input_nivel.value) {
            case 'bajo':
                juego.speed = 10;
                break;
            case 'medio':
                juego.speed = 20;
                break;
            case 'alto':
                juego.speed = 30;
                break;
        }
        localStorage.setItem('nivel', input_nivel.value);
        localStorage.setItem('speed', juego.speed.toString());
    }
}
function play() {
    var speed = parseInt(localStorage.getItem('speed'));
    if (speed) {
        juego.speed = speed;
    }
    if (!juego.playing) {
        animationFrame(juego.speed);
        juego.playing = true;
        juego.setNivel();
        var niveles = document.getElementsByName('nivel');
        for (var i = 0; i < niveles.length; i++) {
            niveles[i].disabled = true;
        }
    }
}
function animationFrame(vel) {
    setInterval(function () {
        juego.actualizar();
        juego.chocar();
        juego.dibujar();
    }, 1000 / vel);
}
var canvas = document.getElementById('canvas');
var contexto = canvas.getContext('2d');
var juego = new Juego(contexto);
var nivel = localStorage.getItem('nivel');
var nombre = localStorage.getItem('nombre');
var maximo_puntaje = localStorage.getItem('maxima_puntuacion');
var puntaje_anterior = localStorage.getItem('puntaje_actual');
if (nivel) {
    var input = document.getElementById(nivel);
    input.checked = true;
}
if (puntaje_anterior) {
    document.getElementById('maxima_puntuacion').innerHTML = '<b>La Maxima Puntuacion: </b></br>'
        + nombre + ' con ' + maximo_puntaje + ' Puntos';
    document.getElementById('ultima_puntuacion').innerHTML = '<b>La Ultima Puntuacion fue de: </br></b>'
        + puntaje_anterior + ' Puntos';
}
document.addEventListener('keydown', function (event) {
    juego.controlarTeclado(event);
});
